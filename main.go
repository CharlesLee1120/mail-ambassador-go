package main

import (
	"crypto/tls"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	gomail "gopkg.in/mail.v2"
	"mail-ambassador-go/dto"
	"net/http"
	"os"
	"regexp"
	"strconv"
)

func main() {

	MAIL_REGEX := `^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$`

	// 1. Get environment variables or parameter
	// Include:
	//   - SMTP server
	//   - SMTP port
	//   - SMTP account username
	//   - SMTP account password

	smtp_server := os.Getenv("MAIL_AMBASSADOR_SMTP_SERVER")
	log.Info().Str("MAIL_AMBASSADOR_SMTP_SERVER", smtp_server).Msg("Get environment variables")

	if smtp_server == "" {
		panic("MAIL_AMBASSADOR_SMTP_SERVER is not setting")
	}

	smtp_port := os.Getenv("MAIL_AMBASSADOR_SMTP_PORT")
	log.Info().Str("MAIL_AMBASSADOR_SMTP_PORT", smtp_port).Msg("Get environment variables")

	if smtp_port == "" {
		panic("MAIL_AMBASSADOR_SMTP_PORT is not setting")
	}
	smtp_port_int, _ := strconv.Atoi(smtp_port)

	smtp_username := os.Getenv("MAIL_AMBASSADOR_SMTP_USERNAME")
	log.Info().Str("MAIL_AMBASSADOR_SMTP_USERNAME", smtp_username).Msg("Get environment variables")

	if smtp_username == "" {
		panic("MAIL_AMBASSADOR_SMTP_USERNAME is not setting")
	}

	smtp_password := os.Getenv("MAIL_AMBASSADOR_SMTP_PASSWORD")
	log.Info().Str("MAIL_AMBASSADOR_SMTP_PASSWORD", smtp_password).Msg("Get environment variables")

	if smtp_password == "" {
		panic("MAIL_AMBASSADOR_SMTP_PASSWORD is not setting")
	}

	// 2. Enable HTTP Server: echo
	e := echo.New()

	// Routes
	e.POST("/mail/send", func(c echo.Context) error {
		mailBody := new(dto.MailBodyDto)

		err := c.Bind(mailBody)
		if err != nil {
			return err
		}

		if mailBody != nil {
			if mailBody.To != nil {
				if len(mailBody.To) > 0 {
					go func() {
						for _, s := range mailBody.To {

							match, _ := regexp.MatchString(MAIL_REGEX, s)
							if match {
								log.Info().Str("From", mailBody.From).Str("To", s).Str("Subject", mailBody.Subject).Msg("Starting send mail")

								// 3. Using gopkg.in/mail.v2 send E-mail
								m := gomail.NewMessage()
								// Set E-Mail sender
								m.SetHeader("From", mailBody.From)
								// Set E-Mail receivers
								m.SetHeader("To", s)
								// Set E-Mail subject
								m.SetHeader("Subject", mailBody.Subject)
								// Set E-Mail body. You can set plain text or html with text/html
								m.SetBody("text/plain", mailBody.Content)
								// Settings for SMTP server
								d := gomail.NewDialer(smtp_server, smtp_port_int, smtp_username, smtp_password)

								// This is only needed when SSL/TLS certificate is not valid on server.
								// In production this should be set to false.
								d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

								// Now send E-Mail
								err := d.DialAndSend(m)

								log.Info().Str("From", mailBody.From).Str("To", s).Str("Subject", mailBody.Subject).Bool("SendStatus", true).Msg("Success send mail")

								if err != nil {
									fmt.Println(err)
									panic(err)

								}

							} else {
								log.Info().Str("From", mailBody.From).Str("To", s).Str("Subject", mailBody.Subject).Bool("SendStatus", false).Msg("Email format error")
							}

						}
					}()

				}
			}
		}

		return c.String(http.StatusOK, "success")
	})

	// Start server
	e.Logger.Fatal(e.Start(":9090"))

}
