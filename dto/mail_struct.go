package dto

type MailBodyDto struct {
	From    string   `json:"from" form:"from" query:"from"`
	To      []string `json:"to" form:"to" query:"to"`
	Subject string   `json:"subject" form:"subject" query:"subject"`
	Content string   `json:"content" form:"content" query:"content"`
}
