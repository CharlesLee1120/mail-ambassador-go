package dto

type MessageDto struct {
	Code    int    `json:"code" form:"code" query:"code"`
	Message string `json:"message" form:"message" query:"message"`
	Data    string `json:"data" form:"data" query:"data"`
}
